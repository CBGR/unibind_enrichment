UniBind Enrichment
==================

The UniBind enrichment tool predicts which sets of TFBSs from the UniBind_
database are enriched in a set of given genomic regions. Enrichment
computations are performed using the LOLA_ tool. For more information about
the underlying enrichment computations, read the `LOLA documentation`_.

Previously, a database of TFBS sets from UniBind was provided in this software
distribution. Since several species-related databases have now been made
available, and since these would increase the size of this distribution
considerably if bundled, it is now necessary to obtain these databases
separately. Instructions for doing so are provided below.

The tool allows for three types of computations:

- Enrichment of TFBSs in a set of genomic regions compared to a given
  universe of genomic regions.
- Differential TFBS enrichment when comparing one set of genomic regions
  (*S1*) to another (*S2*).
- Enrichment of TFBSs in a set of genomic regions compared to all TFBS sets
  stored in UniBind.

.. _LOLA: http://code.databio.org/LOLA/
.. _`LOLA documentation`: http://code.databio.org/LOLA/
.. _UniBind: https://unibind.uio.no/

Prerequisites
-------------

First of all, it is assumed that you will be attempting to use the software on
a Unix-compatible system with a POSIX-compatible shell. To retrieve and
administer this and other software, the ``git`` tool is required, typically
provided by the ``git`` system package.

A deployment script is provided that will perform the retrieval and
installation of system packages as well as programming language-specific
packages where appropriate. The system package details apply to the Fedora
GNU/Linux distribution, but other distributions will use similar package
names.

For more information about the software needed, see the `prerequisites`_
documentation.

.. _`prerequisites`: docs/Prerequisites.rst

Getting Started
---------------

To be able to use this software, you first need to download the software:

.. code:: shell

  git clone https://bitbucket.org/CBGR/unibind_enrichment.git

Now, enter the ``unibind_enrichment`` directory:

.. code:: shell

  cd unibind_enrichment

To automate the configuration and deployment of the software, a script is
provided. To see the activities it will perform, run it as follows:

.. code:: shell

  ./deploy.sh -n -p commandline

Some activities will require elevated privileges obtained using ``sudo``.
Others will be run with normal privileges.

To perform a normal deployment involving some privileged activities to obtain
system packages:

.. code:: shell

  ./deploy.sh -p commandline

Run the script as follows to see more information:

.. code:: shell

  ./deploy.sh --help

Obtaining Necessary Data
------------------------

Several databases can be obtained from the latest Zenodo record for this
project:

https://doi.org/10.5281/zenodo.4161027

These can be downloaded manually from the above Web page. Alternatively, a
program is provided to automate the downloading process for one or more
databases.

To see available databases:

.. code:: shell

  bin/zenodo_fetch 4161027 -l

To download specific databases (such as those for Schizosaccharomyces pombe):

.. code:: shell

  bin/zenodo_fetch 4161027 -f ASM294v2_permissive_UniBind_LOLA.RDS -f ASM294v2_permissive_UniBind_LOLA_universe.RDS

To download all databases:

.. code:: shell

  bin/zenodo_fetch 4161027

Using UniBind Enrichment Tools
------------------------------

A summary of the different operating modes of the tools is given below.

Enrichment within a given universe of genomic regions
+++++++++++++++++++++++++++++++++++++++++++++++++++++

.. image:: img/oneSetBg.png
   :alt: Enrichment with a background

To compute which sets of TFBSs from UniBind are enriched in a set *S* of
genomic regions compared to a universe *U* of genomic regions, you can use the
``oneSetBg`` subcommand as follows.

.. code:: shell

   bin/UniBind_enrich.sh oneSetBg <LOLA db> <S bed> <U bed> <output dir>

This will compute the enrichment of TFBS sets from UniBind in the genomic
regions from *S* (provided as a BED file) when compared to the expectation
from a universe *U* of genomic regions (provided as a BED file). All result
files will be provided in the ``<output dir>`` directory.

.. admonition:: Note

  Every region in *S* should overlap with one region in *U*.

Differential enrichment
+++++++++++++++++++++++

.. image:: img/oneTwoSets.png
   :alt: Differential enrichment

To compute which sets of TFBSs from UniBind are enriched in a set *S1*
of genomic regions compared to another set *S2* of genomic regions, you
can use the ``twoSets`` subcommand as follows.

.. code:: shell

   bin/UniBind_enrich.sh twoSets <LOLA db> <S1 bed> <S2 bed> <output dir>

This will compute the enrichment of TFBS sets from UniBind in the genomic
regions from *S1* (provided as a BED file) when compared to the genomic
regions in *S2* (provided as a BED file). All result files will be provided in
the ``<output dir>`` directory.

Enrichment when no background is provided
+++++++++++++++++++++++++++++++++++++++++

.. image:: img/oneSetNoBg.png
   :alt: Enrichment with no background

When no background is provided, one can compute which sets of TFBSs from
UniBind are enriched in a set *S* of genomic regions using the ``oneSetNoBg``
subcommand as follows. In this case, the enrichment will be computed against a
default background corresponding to the genomic regions of all TFBSs stored in
UniBind.

.. admonition:: Warning

  We encourage users to provide an adequate background set of genomic regions
  whenever possible using the ``oneSetBg`` subcommand.

.. code:: shell

   bin/UniBind_enrich.sh oneSetNoBg <LOLA db> <LOLA universe> <S bed> <output dir>

This will compute the enrichment of TFBS sets from UniBind in the genomic
regions from *S* (provided as a BED file). All result files will be provided
in the ``<output dir>`` directory.

Output
++++++

The output directory will contain the ``allEnrichments.tsv`` file that provides
the enrichment score for each TFBS set from UniBind along with their metadata
information. Similar files (following the template ``col_.tsv``) are created
for each TF with all data sets available for that TF.

A visual representation of the enrichment analysis is provided in the output
directory with three plots:

1. A swarm plot using the log10(p-value) of the enrichment for each TFBS
   set on the y-axis.

   File: ``allEnrichments_swarm.pdf``

2. An interactive `beeswarm plot`_ showing the 10 most enriched TFs.

   File: ``Unibind_enrichment_interactive_beeswarmplot.html``

3. An interactive `ranking plot`_ showing the log10(p-value) for all the
   datasets in UniBind.

   File: ``Unibind_enrichment_interactive_ranking.html``

In the three plots, the data sets for the top 10 TFs showing a log10(p-value)
< 3 are highlighted with dedicated colors (one color per TF). Data sets with
log10(p-value) > 3 are provided with a color for N.S.  (non-significant).

Users can explore the results with the **interactive plots**. These plots
allow to hide/show the top 10 enriched TFs, and the tooltip displays the TF
name, log10(p-value) (also known as significance), and cell type and
treatment.

.. _`beeswarm plot`: https://chart-studio.plot.ly/~jaimicore/23
.. _`ranking plot`: https://chart-studio.plot.ly/~jaimicore/25/#/

Example
+++++++

As an example of application, we provide data derived from the
publication `DNA methylation at enhancers identifies distinct breast
cancer lineages, Fleischer, Tekpli, et al, Nature Communications,
2017 <https://www.nature.com/articles/s41467-017-00510-x>`__. The
genomic regions of interest correspond to 200bp-long regions around CpGs
from cluster 2A described in the publication. These regions around CpGs
of interest are shown to be associated with FOXA1, GATA, and ESR1
binding. We applied the following command to compute TFBS enrichment
using all the CpG probes from the Illumina Infinium HumanMethylation450
microarray:

.. code:: shell

   bin/UniBind_enrich.sh oneSetBg hg38_robust_UniBind_LOLA.RDS \
                                  data/example_Fleischer_et_al/clusterA_200bp_hg38.bed \
                                  data/example_Fleischer_et_al/450k_probes_hg38_200bp.bed \
                                  ub_enrichment

We observe a clear enrichment for TFBSs associated with the expected
TFs. The corresponding swarm plot is:

.. image:: data/example_Fleischer_et_al/allEnrichments_swarm.png
   :alt: Swarm plot

A complementary beeswarm plot allows to clearly visualize the 10 most
enriched TFs.

.. image:: data/example_Fleischer_et_al/Unibind_enrichment_beeswarmplot.jpeg
   :alt: Swarm plot showing the ten most enriched TFs

The enrichment and ranking of all the datasets can be visualized in the
ranking plot.

.. image:: data/example_Fleischer_et_al/Unibind_enrichment_ranking.jpeg
   :alt: Enrichment and ranking of all datasets

Filtering Enrichment Results
----------------------------

Given a set of enrichment results, it is possible to perform additional
filtering to focus on cell lines or types of interest. This is done given an
input directory of results and a nominated output directory as follows:

.. code:: shell

   bin/UniBind_filter.sh <output dir> <input dir> <cell type>...

For example:

.. code:: shell

   bin/UniBind_filter.sh ube_filtered ub_enrichment 'MCF7 (Invasive ductal breast carcinoma)'

To avoid problems specifying cell types, it is recommended that each value is
quoted as shown above.

The filtering program selects only the result records containing any of the
indicated cell types and then produces new plots from the selected records,
depositing these in the output directory together with the filtered results.
To facilitate inspection of the effects of filtering, a different directory
must be specified for output than that providing the input.
