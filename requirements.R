# This must be run with R_LIBS referencing the appropriate directory.

required <- c("devtools", "BiocManager", "data.table", "dplyr",
              "ggbeeswarm", "ggplot2", "htmlwidgets", "optparse",
              "plotly", "RColorBrewer", "reshape2", "usethis")

location <- .libPaths()[1]

to_install <- setdiff(required, rownames(installed.packages(location)))

if (length(to_install)) {
    install.packages(to_install, repos="https://cran.uib.no/")
}

BiocManager::install(c("BiocGenerics", "S4Vectors", "IRanges", "GenomicRanges"))
