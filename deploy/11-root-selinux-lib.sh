#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

. "$THISDIR/tools/system.sh"

if ! have semanage ; then
    exit 0
fi

# Initialise directories for the installed libraries.

LOCALDIR="$BASEDIR/lib/usr/local"

# Set labels for SELinux, apply the labelling for SELinux.

semanage fcontext -a -t bin_t "$LOCALDIR/bin(/.*)?"
semanage fcontext -a -t lib_t "$LOCALDIR/lib(/.*)?"
semanage fcontext -a -t lib_t "$LOCALDIR/lib64(/.*)?"
restorecon -R "$LOCALDIR"

# vim: tabstop=4 expandtab shiftwidth=4
