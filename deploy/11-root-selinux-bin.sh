#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

. "$THISDIR/tools/system.sh"

if ! have semanage ; then
    exit 0
fi

# Label the wrapper scripts.

ENRICH="$BASEDIR/bin/UniBind_enrich.sh"
FILTER="$BASEDIR/bin/UniBind_filter.sh"

semanage fcontext -a -t bin_t "$ENRICH"
semanage fcontext -a -t bin_t "$FILTER"
restorecon "$ENRICH"
restorecon "$FILTER"

# vim: tabstop=4 expandtab shiftwidth=4
