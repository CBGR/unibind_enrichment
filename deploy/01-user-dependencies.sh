#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

. "$THISDIR/tools/system.sh"

# If the idiocy of Python packaging isn't irritating enough, Debian also
# apparently introduces non-standard behaviour to its pip package, effectively
# enforcing the --user option even when --root is specified (and it obstructs
# --prefix as well).
#
# See: https://github.com/pypa/pip/issues/1668

if system_is_debian ; then
    SYSTEM="--system"
else
    SYSTEM=
fi

# Initialise local dependencies.

python3 -m pip install $SYSTEM --ignore-installed --root "$BASEDIR/lib" -r "$BASEDIR/requirements.txt"

# Fix up paths for Debian.

if system_is_debian ; then
    for DIST_PACKAGES in `find "$BASEDIR/lib" -name 'dist-packages' -type d` ; do
        LIBDIR=`dirname "$DIST_PACKAGES"`
        SITE_PACKAGES="$LIBDIR/site-packages"
        if [ ! -e "$SITE_PACKAGES" ] ; then
            ln -s 'dist-packages' "$SITE_PACKAGES"
        fi
    done
fi

# R dependencies.

mkdir -p "$BASEDIR/lib/usr/local/lib/R/library"
R_LIBS="$BASEDIR/lib/usr/local/lib/R/library" Rscript "$BASEDIR/requirements.R"

# vim: tabstop=4 expandtab shiftwidth=4
