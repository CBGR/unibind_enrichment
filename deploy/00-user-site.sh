#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Obtain any data location and use the indicated values instead of the default.

DATA=
DATA_DEFAULT="/var/tmp/unibind-data"

while getopts d: NAME ; do
    case "$NAME" in
        d) DATA=$OPTARG ;;
        *) echo "Option ignored." 1>&2 ;;
    esac
done

shift $((OPTIND - 1))

# Record the data details for other operations.

if [ ! -e "$THISDIR/work" ] ; then
    mkdir -p "$THISDIR/work"
fi

DATA_FILE="$THISDIR/work/data.txt"

if [ "$DATA" ] || [ ! -e "$DATA_FILE" ] ; then
    echo ${DATA:-$DATA_DEFAULT} > "$DATA_FILE"
fi

# vim: tabstop=4 expandtab shiftwidth=4
