#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

. "$THISDIR/tools/system.sh"

# Test for the required program and exit if it is found.

if have bedtools ; then
    exit 0
fi

if [ -e "$BASEDIR/lib/usr/local/bin/bedtools" ] ; then
    exit 0
fi

# Obtain bedtools.

WORKDIR="$THISDIR/work"
CLONEDIR="$WORKDIR/bedtools2"

mkdir "$WORKDIR"
git clone https://github.com/arq5x/bedtools2.git "$CLONEDIR"

# Patch bedtools.

patch -N -d "$CLONEDIR" -p0 < "$THISDIR/patches/patch-bedtools-scripts.diff"

# Build and install bedtools.

cd "$CLONEDIR"
make
make install DESTDIR="$BASEDIR/lib"

# vim: tabstop=4 expandtab shiftwidth=4
