#!/bin/sh

# SPDX-FileCopyrightText: 2019-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-FileContributor: jaimicore <jcastro@lcg.unam.mx>
# SPDX-FileContributor: Anthony Mathelier <anthony.mathelier@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

PROGNAME=$(basename $0)
THISDIR=$(dirname $0)



# Location of required libraries.

LIB=$(realpath "$THISDIR/../lib")

. "$THISDIR/python_env.sh"

# Define appropriate program and library locations for local software
# installations. Otherwise, use system software installations.

if [ -e "$LIB" ] ; then

    # Make locally installed R libraries available.

    PREFIX="$LIB/usr/local"
    export R_LIBS="$PREFIX/lib/R/library"

    # Make locally installed programs available.

    PATH="$PREFIX/bin:$PATH"
fi


# Result columns.

CellColNum=16

# Obtain cell line summary.
# cell_summary <output directory>

cell_summary() {
    tail -n +2 "$1/allEnrichments.tsv" | cut -f "$CellColNum" | sort -u > "$1/cell_summary"
}



# Plot charts for the output data.
# plot <output directory>

plot() {
    $PYTHON $THISDIR"/swarm_plot.py" \
       -f "$1/allEnrichments.tsv" \
       -o "$1/allEnrichments_swarm.pdf"

    Rscript $THISDIR"/UniBind_enrichment_swarmplot.R" \
        -t "$1/allEnrichments.tsv" \
        -o "$1"
}



# Subcommand functions.

sub_help() {
    echo "Usage: $PROGNAME <subcommand> [options]\n"
    echo "Subcommands:"
    echo "    oneSetBg   Apply one set enrichment analysis against a background"
    echo "    usage: $PROGNAME oneSetBg <loladb> <bed file> <universe/background bed> <output dir>"
    echo "    oneSetNoBg   Apply one set enrichment analysis"
    echo "    usage: $PROGNAME oneSetNoBg <loladb> <lola universe> <bed file> <output dir>"
    echo "    twoSets  Apply two sets differential enrichment analysis"
    echo "    usage: $PROGNAME twoSets <loladb> <1st bed file> <2nd bed file> <output dir>"
    echo ""
    echo "For help with each subcommand run:"
    echo "$PROGNAME <subcommand> -h|--help"
    echo ""
}

sub_oneSetBg() {
    echo "Running 'oneSetBg' analysis"
    tmpin=$(mktemp)
    bedtools sort -i "$2" | bedtools merge -i stdin > "$tmpin"
    tmpbg=$(mktemp)
    bedtools sort -i "$3" | bedtools merge -i stdin > "$tmpbg"

    if R --silent --slave --vanilla -f "$THISDIR/lola.R" \
         --args "$1" "$tmpin" "$tmpbg" "$4" "$THISDIR/LOLA_modif" ; then

        plot "$4"
        cell_summary "$4"
        STATUS=0
    else
        STATUS=$?
    fi

    rm $tmpin $tmpbg
    return $STATUS
}

sub_oneSetNoBg() {
    echo "Running 'oneSetNoBg' analysis"
    tmpin=$(mktemp)
    bedtools sort -i "$3" | bedtools merge -i stdin > "$tmpin"

    if R --silent --slave --vanilla -f "$THISDIR/lola_no_background.R" \
         --args "$1" "$2" "$tmpin" "$4" "$THISDIR/LOLA_modif" ; then

        plot "$4"
        cell_summary "$4"
        STATUS=0
    else
        STATUS=$?
    fi

    rm $tmpin
    return $STATUS
}

sub_twoSets() {
    echo "Running 'twoSets' analysis"
    tmpone=$(mktemp)
    tmptwo=$(mktemp)
    bedtools subtract -a "$2" -b "$3" | bedtools sort -i stdin | \
        bedtools merge -i stdin > "$tmpone"
    bedtools subtract -b "$2" -a "$3" | bedtools sort -i stdin | \
        bedtools merge -i stdin > "$tmptwo"

    if R --silent --slave --vanilla -f "$THISDIR/lola_two_sets.R" \
         --args "$1" "$tmpone" "$tmptwo" "$4" "$THISDIR/LOLA_modif" ; then

        plot "$4"
        cell_summary "$4"
        STATUS=0
    else
        STATUS=$?
    fi

    rm $tmpone $tmptwo
    return $STATUS
}



# Main program.

# Interpret the subcommand.

SUBCOMMAND=$1
shift

case $SUBCOMMAND in

    # Help message.

    "" | "-h" | "--help")
        sub_help
        ;;

    # Operation subcommands.

    *)
        # Make any output directory.

        if [ ! -e "$4" ] ; then
            mkdir -p "$4"
        fi

        # Invoke recognised subcommands.

        IFS=
        sub_${SUBCOMMAND} $@
        STATUS=$?

        # Upon success, write a special file indicating this condition, exiting
        # with the success status code.

        if [ $STATUS = 0 ] ; then
            touch "$4/success"
            exit 0
        fi

        # Upon failure, write a special file indicating this condition.

        touch "$4/failed"

        # The special status code of 127 indicates that the subcommand function
        # was not recognised.

        if [ $STATUS = 127 ] ; then
            echo "Error: '$SUBCOMMAND' is not a known subcommand." >&2
            echo "       Run '$PROGNAME --help' for a list of known subcommands." >&2
            exit 1
        fi

	exit $STATUS
        ;;
esac
