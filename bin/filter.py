#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Filter enrichment results using lists of permissible values corresponding to
particular result columns.
"""

import sys

def read_values(filename):

    "Read values from 'filename' and return them in a set."

    f = open(filename)
    try:
        values = set()
        for line in f.readlines():
            value = line.strip()
            if value:
                values.add(value)
        return values
    finally:
        f.close()

def read_record(infile):

    """
    Return a record from 'infile' as a sequence of columns or None if no record
    is present.
    """

    line = infile.readline()
    columns = line.strip()
    if columns:
        return columns.split("\t")
    else:
        return None

def write_record(record, outfile):

    "Write 'record' to 'outfile'."

    outfile.write("\t".join(record) + "\n")

def filter_results(infile, value_sets, outfile):

    """
    Filter results from 'infile' using the 'value_sets' mapping containing
    column-number/value-set correspondences. Produce processed results via
    'outfile'.
    """

    record = read_record(infile)
    while record:

        # Try each value collection for the appropriate columns. Upon finding a
        # permissible value, stop searching for permissible values and write the
        # result.

        for colnum, values in value_sets.items():
            if record[colnum] in values:
                write_record(record, outfile)
                break

        # Read another record.

        record = read_record(infile)

# Invocation support.

help_text = """\
Usage: %s ( <column number> <filename> )...

Filter results provided by standard input, applying collections of permissible
values to the indicated columns, producing results on standard output if any of
the result column values appears in the corresponding list of values.

Note that the column numbers are 1-based.
""" % sys.argv[0]

def main(args):

    "Perform result filtering using the given 'args'."

    if args and args[0] == "--help":
        print(help_text)
        return

    value_sets = {}

    while args:
        colnum, filename = args[:2]
        del args[:2]
        value_sets[int(colnum) - 1] = read_values(filename)

    filter_results(sys.stdin, value_sets, sys.stdout)

if __name__ == "__main__":
    main(sys.argv[1:])

# vim: tabstop=4 expandtab shiftwidth=4
