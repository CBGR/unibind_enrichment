#!/bin/sh

# SPDX-FileCopyrightText: 2019-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-FileContributor: jaimicore <jcastro@lcg.unam.mx>
# SPDX-FileContributor: Anthony Mathelier <anthony.mathelier@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

PROGNAME=$(basename $0)
THISDIR=$(dirname $0)



# Location of required libraries.

LIB=$(realpath "$THISDIR/../lib")

. "$THISDIR/python_env.sh"

# Define appropriate program and library locations for local software
# installations. Otherwise, use system software installations.

if [ -e "$LIB" ] ; then

    # Make locally installed R libraries available.

    PREFIX="$LIB/usr/local"
    export R_LIBS="$PREFIX/lib/R/library"

    # Make locally installed programs available.

    PATH="$PREFIX/bin:$PATH"
fi


# Result columns.

CellColNum=16

# Result files.

ResultFileBase="allEnrichments"
ResultFile="$ResultFileBase.tsv"

# Obtain cell line summary.
# cell_summary <output directory>

cell_summary() {
    tail -n +2 "$1/$ResultFile" | cut -f "$CellColNum" | sort -u > "$1/cell_summary"
}



# Filter by cell type.
# filter <output directory> <input directory> <cell type>...

filter() {
    OutDir=$1
    InDir=$2
    shift 2

    # Prevent overwriting of the input data.

    if [ "$InDir" == "$OutDir" ] ; then
        cat 1>&2 <<EOF
The same directory was given for both the input and output directory. Since this
would overwrite the inputs and make the effect of filtering difficult to
measure, filtering will not be performed.
EOF
        return 1
    fi

    echo "Running filter" 1>&2

    # Write cell types to a file.

    CellTypeFile="$OutDir/cell_types"
    echo -n > "$CellTypeFile"

    for CellType in $* ; do
        echo "$CellType" >> "$CellTypeFile"
    done

    # Obtain the header and body.

    head -n 1 "$InDir/$ResultFile" > "$OutDir/$ResultFile.head"
    tail -n +2 "$InDir/$ResultFile" > "$OutDir/$ResultFile.body"

    # Filter the body, if requested, concatenating the header and filtered body.

      "$PYTHON" "$THISDIR/filter.py" "$CellColNum" "$CellTypeFile" \
    < "$OutDir/$ResultFile.body" > "$OutDir/$ResultFile.filtered"

    # Replace the results with filtered results only if there are some
    # filtered results. Otherwise, the empty filtered results will be left
    # as an artefact.

    if [ `stat -c "%s" "$OutDir/$ResultFile.filtered"` != '0' ] ; then
        cat "$OutDir/$ResultFile.head" "$OutDir/$ResultFile.filtered" > "$OutDir/$ResultFile"
        rm "$OutDir/$ResultFile.filtered"

    # Copy the original results to the output directory where no filtered
    # results are produced. This permits chart generation for the unfiltered
    # results instead.

    elif [ -e "$InDir/$ResultFile" ] && [ ! -e "$OutDir/$ResultFile" ] ; then
        cp "$InDir/$ResultFile" "$OutDir/$ResultFile"
    fi

    rm "$OutDir/$ResultFile.head" "$OutDir/$ResultFile.body"
}

# Plot charts for the output data.
# plot <output directory>

plot() {
    $PYTHON "$THISDIR/swarm_plot.py" \
       -f "$1/$ResultFile" \
       -o "$1/${ResultFileBase}_swarm.pdf"

    Rscript "$THISDIR/UniBind_enrichment_swarmplot.R" \
        -t "$1/$ResultFile" \
        -o "$1"
}



# Subcommand functions.

sub_help() {
    cat <<EOF
Usage: $PROGNAME <output dir> <input dir> <cell type>...

Produce in the indicated output directory, using enrichment results found in the
given input directory, a collection of filtered enrichment results involving
only the specified cell lines or types.

Each cell type is a textual label corresponding to those appearing in the
enrichment results file:

$ResultFile

Since these labels may contain spaces, care must be taken to quote them
appropriately when supplying them to this program. For example:

$PROGNAME ube_filtered ub_enrichment 'MCF7 (Invasive ductal breast carcinoma)'
EOF
}

sub_filter() {
    filter $* && plot "$1" && cell_summary "$1"
}



# Main program.

OPTION=$1

case $OPTION in

    # Help message.

    "" | "-h" | "--help")
        sub_help
        ;;

    # Normal invocation.

    *)
        # Make any output directory.

        if [ ! -e "$1" ] ; then
            mkdir -p "$1"
        fi

        # Pass the cell types together with the directories. Since cell types
        # may contain spaces, argument tokenisation is disabled.

        IFS=
        sub_filter $*

        # Upon success, write a special file indicating this condition, exiting
        # with the success status code.

        if [ $? = 0 ] ; then
            touch "$1/success"
            exit 0
        fi

        # Upon failure, write a special file indicating this condition.

        touch "$1/failed"
        ;;
esac
