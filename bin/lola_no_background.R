args = commandArgs(trailingOnly=T)
loladb = args[1]
lolauniverse = args[2]
inputbed = args[3]
outputfolder = args[4]
loladir = args[5]

library(devtools)
devtools::load_all(loladir)
library(GenomicRanges)

regionDB = readRDS(loladb)
regions = readBed(inputbed)
universe = readRDS(lolauniverse)
locresults = runLOLA(regions, universe, regionDB)
writeCombinedEnrichment(locresults, outFolder=outputfolder,
                        includeSplits=TRUE)
saveRDS(locresults, file=paste0(outputfolder, "/lolaLocResults.RDS"))
